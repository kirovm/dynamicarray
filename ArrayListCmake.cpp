﻿// ArrayListCmake.cpp : Defines the entry point for the application.
//

#include "ArrayListCmake.h"
#include "arraylist/Arraylist.h"

using namespace std;


int main()
{
	Arraylist* arr = new Arraylist();
	arr->pushBack(10);
	Arraylist temp = *arr;
	Arraylist temp2;
	temp2 = *arr;
	int i = arr->at(0);
	cout << "First elem in arr is " << arr->at(0) << endl;
	cout << "First elem in temp is " << temp.at(0) << endl;
	cout << "First elem in temp2 is " << temp2.at(0) << endl;
	return 0;
}
