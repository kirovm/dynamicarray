#include "Arraylist.h"


using namespace std;

const double Arraylist::CAPACITY_MULTIPLIER = 2.0;
const size_t Arraylist::START_CAPACITY = 10;

void Arraylist::clearState() {
	this->size = 0;
	this->capacity = 0;
}

void Arraylist::destroy() {
	clearState();
	MyAllocator::deallocate(this->arr);
}

Arraylist::Arraylist()
{
	//printf("Default constr\n");
	clearState();
	this->arr = MyAllocator::allocate(Arraylist::START_CAPACITY);

	//clear state if allocation has failed
	if (this->arr == nullptr)
		return;

	this->capacity = Arraylist::START_CAPACITY;
}


Arraylist::Arraylist(const Arraylist& other)
{
	copyArraylist(other);
}

Arraylist& Arraylist::operator=(const Arraylist& other)
{
	//printf("Operator =\n");
	if (this == &other)
		return *this;

	//REVIEW: guarantees that the state will be set to "clear"
	// if the operation fails.
	//TODO how to predict unpredicted behaviour if memory has not been allocated at first?
	destroy();
	copyArraylist(other);
	return *this;
}

void Arraylist::copyArraylist(const Arraylist& other) {
	this->arr = MyAllocator::allocate(other.capacity);
	if (arr == nullptr) {
		//if allocation has filed clearState
		clearState();
		return;
	}

	this->capacity = other.getCurrentCapacity();
	this->size = other.getSize();
	std::copy(other.arr, other.arr + other.size, this->arr);
}

Arraylist::~Arraylist()
{
	destroy();
}

void Arraylist::pushBack(int value)
{
	if (this->capacity == 0) {
		//bad allocation in array creation, do nothing
		return;
	}

	if (this->size < this->capacity) {
		//there is enough place in the array
		this->arr[size] = value;
		this->size++;
		return;
	}

	//create tempBuffer
	size_t tempBufferSize = this->capacity * Arraylist::CAPACITY_MULTIPLIER;
	int* tempBuffer = MyAllocator::allocate(tempBufferSize);

	//copy content of original buffer in temp
	std::copy(this->arr, this->arr + this->size, tempBuffer);

	//delete all allocated elements in original one
	MyAllocator::deallocate(arr);

	//make original buffer point to temp
	this->arr = tempBuffer;

	//add element in the end
	this->arr[size] = value;
	this->size++;
	this->capacity = tempBufferSize;
}

void Arraylist::popBack()
{
	if(this->size > 0)
		this->size--;
}

int& Arraylist::at(size_t index)
{
	if (index >= this->getSize()) {
		throw std::out_of_range("You are past the end of the array");
	}

	return this->arr[index];
}

int& Arraylist::operator[](size_t index)
{

	return arr[index];
}

size_t Arraylist::getSize() const
{
	return this->size;
}

size_t Arraylist::getCurrentCapacity() const
{
	return this->capacity;
}

bool Arraylist::isEmpty() const
{
	return this->size == 0;
}

void Arraylist::clear()
{
	this->size = 0;
}

double Arraylist::getCapacityMultiplier() {
	return Arraylist::CAPACITY_MULTIPLIER;
}

size_t Arraylist::getCapacityOnCreation() {
	return Arraylist::START_CAPACITY;
}

bool Arraylist::isEqual(const Arraylist& other) {
	if (this->size != other.getSize() || this->capacity != other.getCurrentCapacity())
		return false;

	for (int i = 0; i < this->size; i++) {
		if (this->arr[i] != other.arr[i])
			return false;
	}

	return true;
}

void Arraylist::printArray() {
	for (int i = 0; i < this->size; i++) {
		cout << "arr[" << i << "] = " << this->arr[i] << endl;
	}
}