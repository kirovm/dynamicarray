#pragma once
#include <stdlib.h>
#include <new>
#include <vcruntime_string.h>
#include <iostream>
#include "MyAllocator.h"

/**
 * @brief  dynamic array class. The array contains variables of type int. 
   The interface is close to that of the std::vector class in STL
*/
class Arraylist {
private:
	static const size_t START_CAPACITY;
	static const double CAPACITY_MULTIPLIER;
	int* arr;
	size_t size;
	size_t capacity;
	void copyArraylist(const Arraylist& other);

	/**
	 * @brief clear state and destroy all allocated memory in the array
	*/
	void destroy();

	/**
	 * @brief sets size and capacity to 0
	*/
	void clearState();

public:
	Arraylist();
	Arraylist(const Arraylist& other);
	Arraylist& operator=(const Arraylist& other);
	~Arraylist();
	
	/**
	 * @brief Push value in back of the array
	 * @param value value to be pushed in the back of the array.
	 *	If there is not enough space, function will allocate size * SIZE_MULTIPLIER array, and coppy information in it.
	 *  If there is enough space, value will be placed in after the last element in the array
	*/
	void pushBack(int value);

	/**
	 * @brief Removes the last element in the array by decreasing size of the arraylist
	 * The element is still there, but if you push back after popBack, you will overwrite it!
	*/
	void popBack();

	/**
	 * @brief 
	 * @param index index of desired element
	 * @retrn desired element
	 * @throw Could throw out_of_range exception if index > arraylist capacity
	*/
	int& at(size_t index);	//throws exception

	/**
	 * @brief take element in position
	 * @param index desired positition in arraylist
	 * @return element at desired position
	 * @warning if index > arraylist size, returned value will be value, which is writed in this addres(unpredicted behaviour)
	*/
	int& operator[](size_t index);	//does not throw exception

	/**
	 * @brief get how many elements there are in the array
	*/
	size_t getSize()const;

	size_t getCurrentCapacity()const;

	bool isEmpty()const;

	/**
	 * @brief only set size to 0
	*/
	void clear();

	double getCapacityMultiplier();

	size_t getCapacityOnCreation();

	/**
	 * @brief check if current arraylist is equals with other.
	 * We assume two arraylists are equals, if they have same size, capacity and elements	 
	*/
	bool isEqual(const Arraylist& other);

	void printArray();
};