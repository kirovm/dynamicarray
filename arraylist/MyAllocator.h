#pragma once
#include <stdlib.h>
#include <iostream>
#include <unordered_set>
/**
 * @brief Allocator for int arrays
*/
class MyAllocator
{
private:
    static std::unordered_set<int*> allocated;
public:
    /**
     * @brief Allocates memory of lenght ints
     * @param lenght 
     * @return allocated memory address
    */
    static int* allocate(size_t length) {
        //printf("Allocation request\n");
        int* allocated = (int*)malloc(sizeof(int) * length);
        //printf("Alocate %ld\n", allocated);
        MyAllocator::allocated.insert(allocated);
        return allocated;
    }

    /**
	 * @brief clear state and destroy all allocated memory in the array
	 * @warning if memory have not been allocated at first, dealocator may cause unpredicted behaviour
     * @param ptr addres of first allocated memory
    */
    static void deallocate(int* ptr) {
        //printf("Dealocate %ld\n", ptr);
        //printf("Deallocation request\n");
        std::unordered_set<int*>::iterator currElem = MyAllocator::allocated.find(ptr);
        if (currElem == MyAllocator::allocated.end()) {
            printf("---WARNING---\nMemory you are trying to free has not been allocated\n");
            return;
        }

        //we have allocated memory
        MyAllocator::allocated.erase(currElem);
        free(ptr);
    }

    static size_t getAllocatedItemsLen() {
        return MyAllocator::allocated.size();
    }

    static void printAllocatedItems() {
        printf("Allocated elements are:\n");
        std::unordered_set<int*>::iterator i = MyAllocator::allocated.begin();
        for (i; i != MyAllocator::allocated.end(); i++) {
            printf("%p,", *i);
        }
        printf("\n");
    }
};