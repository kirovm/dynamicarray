project(Tests)

cmake_minimum_required (VERSION 3.8)

add_executable (Tests "Test.cpp" "../arraylist/Arraylist.cpp" "../arraylist/MyAllocator.cpp")
find_package(Catch2 REQUIRED)
target_link_libraries(Tests Catch2::Catch2)

enable_testing()
add_test(T1 tests)