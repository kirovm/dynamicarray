#define CATCH_CONFIG_MAIN
#include "../arraylist/Arraylist.h"
#include <catch2/catch.hpp>
#include "../arraylist/MyAllocator.h"

//test constructor
SCENARIO("Arraylists can be Created", "[vector]") {
	Arraylist* p;
	REQUIRE_NOTHROW(p = new Arraylist());
	delete p;
}

//test pushBack
SCENARIO("Single element could be pushed in arraylist") {
	Arraylist* arr = new Arraylist();
	WHEN("Add element") {
		arr->pushBack(5);

		THEN("Last element is last added") {
			REQUIRE((arr->at(arr->getSize() - 1)) == 5);
		}
	}
	delete arr;
}


//test push back worst case scenario
SCENARIO("Add more than capacity * SIZE_MULTIPLIER") {
	Arraylist* arr = new Arraylist();
	int i = 0;
	//fill the array
	for (i; i < arr->getCurrentCapacity(); i++) {
		arr->pushBack(i);
	}

	WHEN("add new element") {
		arr->pushBack(i);

		THEN("Capacity should be modified") {
			REQUIRE(arr->getCurrentCapacity() == arr->getCapacityOnCreation() * arr->getCapacityMultiplier());
		}

		for (int j = 0; j < i; j++) {
			THEN("All elements should be added") {
				REQUIRE(arr->at(j) == j);
			}
		}
	}

	delete arr;
}


//test popBack
SCENARIO("PopBack last element in the arraylist") {
	Arraylist* arr = new Arraylist();
	int initialSize = 10;

	//test popBack in case of empty array
	WHEN("Pop back element from empty array") {
		REQUIRE_NOTHROW(arr->popBack());

		THEN("Array size should be 0") {
			REQUIRE(arr->getSize() == 0);
		}
	}

	WHEN("Pop back element") {
		for (int i = 0; i < initialSize; i++) {
			arr->pushBack(i);
		}

		arr->popBack();

		THEN("Array size should be smaller by one") {
			REQUIRE(arr->getSize() == initialSize - 1);
		}
	}

	delete arr;
}


//test at and []
SCENARIO("Take Nth element in the arraylist") {
	Arraylist* arr = new Arraylist();
	arr->pushBack(5);

	WHEN("Get existing eleent") {
		REQUIRE(arr->at(0) == 5);
		REQUIRE((*arr)[0] == 5);
	}

	WHEN("Get element out of range") {
		CHECK_THROWS_AS(arr->at(arr->getCurrentCapacity() + 1), std::out_of_range);
		CHECK_NOTHROW((*arr)[arr->getCurrentCapacity() + 1]);
	}

	delete arr;
}



//test isEmpty
SCENARIO("Check if arraylist is empty") {
	Arraylist* arr = new Arraylist();

	WHEN("Arraylist is empty") {
		REQUIRE(arr->isEmpty() == true);
	}

	WHEN("Arraylist is not empty") {
		arr->pushBack(10);
		REQUIRE(arr->isEmpty() == false);
	}

	delete arr;
}


//test clear
SCENARIO("Test clear funciton") {
	Arraylist* arr = new Arraylist();
	arr->pushBack(5);
	
	WHEN("Array is cleared") {
		arr->clear();
		REQUIRE(arr->isEmpty() == true);
		REQUIRE(arr->getSize() == 0);
	}

	delete arr;
}

//test getSIze
SCENARIO("Change arraylist size") {
	Arraylist* arr = new Arraylist();
	size_t initialSize = arr->getSize();

	WHEN("Add element") {
		arr->pushBack(10);

		THEN("Size increases by 1") {
			REQUIRE(arr->getSize() == initialSize + 1);
		}
	}

	delete arr;
}

//test isEqual
SCENARIO("element could be created, using existing one") {
	Arraylist* arr = new Arraylist();
	Arraylist* arr2 = new Arraylist();
	int size = 100;
	for (int i = 0; i < size; i++) {
		arr->pushBack(i);
		arr2->pushBack(i);
	}

	THEN("Check if two arrays are equal") {
		REQUIRE(arr->isEqual(*arr2) == true);
	}

	delete arr;
	delete arr2;
}

SCENARIO("Copy an array")
{
	GIVEN("An array of [1,2,3,4,5]")
	{
		Arraylist arr;
		arr.pushBack(1);
		arr.pushBack(2);
		arr.pushBack(3);
		arr.pushBack(4);
		arr.pushBack(5);

		WHEN("we copy it")
		{
			Arraylist copy(arr);

			THEN("the size of the copy is the same as the original")
			{
				REQUIRE(arr.getSize() == copy.getSize());
			}
			
			THEN("the capacity of the copy is the same as the original")
			{
				REQUIRE(arr.getCurrentCapacity() == copy.getCurrentCapacity());
			}
			THEN("the elements and their order in the copy are the same as in the original")
			{
				for (int i = 0; i < arr.getSize(); ++i)
					REQUIRE(arr[i] == copy[i]);
			}
		}

		WHEN("we use operator =")
		{
			Arraylist copy = arr;

			THEN("the size of the copy is the same as the original")
			{
				REQUIRE(arr.getSize() == copy.getSize());
			}

			THEN("the capacity of the copy is the same as the original")
			{
				REQUIRE(arr.getCurrentCapacity() == copy.getCurrentCapacity());
			}
			THEN("the elements and their order in the copy are the same as in the original")
			{
				for (int i = 0; i < arr.getSize(); ++i)
					REQUIRE(arr[i] == copy[i]);
			}
		}

		WHEN("we use operator =")
		{
			Arraylist copy;
			copy = arr;

			THEN("the size of the copy is the same as the original")
			{
				REQUIRE(arr.getSize() == copy.getSize());
			}

			THEN("the capacity of the copy is the same as the original")
			{
				REQUIRE(arr.getCurrentCapacity() == copy.getCurrentCapacity());
			}
			THEN("the elements and their order in the copy are the same as in the original")
			{
				for (int i = 0; i < arr.getSize(); ++i)
					REQUIRE(arr[i] == copy[i]);
			}
		}
	}
}

SCENARIO("Allocate new memory only when it is needed") {
	GIVEN("Empty array") {
		Arraylist arr;

		WHEN("Enlarge the array certain amount of times") {
			size_t startCapacity = arr.getCapacityOnCreation();
			int timesEnlargeArray = 10;
			int elementsCounter = 0;
			for (int i = 0; i < timesEnlargeArray; i++) {
				size_t numberOfElementsToBeAddedUntillArrIsFull = arr.getCurrentCapacity() - arr.getSize();
				for (size_t i = 0; i <= numberOfElementsToBeAddedUntillArrIsFull; i++) {
					arr.pushBack(elementsCounter);
					//printf("Push back%d\n", ((int)i));
					elementsCounter++;
				}
			}

			THEN("set of allocted memories shold be 1") {
				REQUIRE(MyAllocator::getAllocatedItemsLen() == 1);
			}

			THEN("Array should have following elements") {
				for (int i = 0; i < arr.getSize(); i++) {
					REQUIRE(arr[i] == i);
				}
			}
		}
	}

	THEN("All memory should be free") {
		REQUIRE(MyAllocator::getAllocatedItemsLen() == 0);
	}
}

SCENARIO("All elements should be destroyed") {
	THEN("Length of allocated elements should be 0") {
		REQUIRE(MyAllocator::getAllocatedItemsLen() == 0);
	}
}